#!/bin/sh

# See https://github.com/weaveworks/eksctl/releases for the latest release version
EKSCTL_VERSION=0.1.35

echo "Installing eksctl v${EKSCTL_VERSION}"
# https://eksctl.io/
curl --silent --location "https://github.com/weaveworks/eksctl/releases/download/${EKSCTL_VERSION}/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
mv /tmp/eksctl /usr/local/bin

which eksctl
